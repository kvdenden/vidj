Rails.application.routes.draw do
  devise_for :users

  resources :channels, only: [:new, :create, :show]

  get 'channels/search' => 'channels#search', as: :search_channel
  get 'channels/:id/playlist' => 'channels#playlist', as: :channel_playlist
  post 'channels/:id/add' => 'channels#add_video', as: :add_video
  post 'channels/:id/:video_id/upvote' => 'channels#upvote_video', as: :upvote_video
  post 'channels/:id/:video_id/downvote' => 'channels#downvote_video', as: :downvote_video
  post 'channels/:id/:video_id/unvote' => 'channels#unvote_video', as: :unvote_video
  get 'channels/:id/play' => 'channels#play_video', as: :play_video
  post 'channels/:id/next' => 'channels#next_video', as: :next_video
  get 'channels/:id/search' => 'channels#search_video', as: :search_video
  delete 'channels/:id/:video_id' => 'channels#remove_video', as: :remove_video
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
