class CreateChannels < ActiveRecord::Migration
  def change
    create_table :channels do |t|
      t.references :user, index: true
      t.string :title, null: false
      t.text :description
      t.string :slug, null: false

      t.timestamps
    end
    add_index :channels, :slug, unique: true
  end
end
