class AddPlayedAtToVideos < ActiveRecord::Migration
  def change
    add_column :videos, :played_at, :datetime
  end
end
