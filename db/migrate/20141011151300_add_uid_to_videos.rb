class AddUidToVideos < ActiveRecord::Migration
  def change
    add_column :videos, :uid, :string
    remove_column :videos, :link
    remove_column :videos, :description
  end
end
