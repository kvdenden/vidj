class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.references :user, index: true
      t.references :channel, index: true
      t.string :title
      t.text :description
      t.string :link

      t.timestamps
    end
  end
end
