class ChannelsController < ApplicationController
  before_action :get_channel, except: [:new, :create]

  def new
    @channel = Channel.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @channel }
    end
  end

  def create
    @channel = current_or_guest_user.channels.build(channel_params)
    respond_to do |format|
      if @channel.save
        format.html { redirect_to @channel, :notice => 'Channel was successfully created.' }
      else
        format.html { render :action => "new" }
      end
    end
  end

  def search
    if @channel
      format.html { redirect_to @channel }
    else
      format.html { redirect_to root_url, :notice => 'Channel does not exist.' }
    end
  end

  def show
    respond_to do |format|
      format.html # show.html.erb
    end
  end

  def destroy
    respond_to do |format|
      format.html { redirect_to root_path }
    end
  end

  def add_video
    active = @channel.active_video
    @video = @channel.videos.create(title: params[:title], uid: params[:uid], user: current_or_guest_user)
    respond_to do |format|
      format.html { redirect_to @channel }
      format.js {
        flash[:success] = "#{@video.title} was added to the playlist"
        if active != @channel.active_video
          render action: "broadcast_update_active_video"
        else
          render action: "broadcast_update_playlist"
        end
      }
    end
  end

  def remove_video
    video = Video.find(params[:video_id])
    video.destroy

    respond_to do |format|
      format.html { redirect_to @channel }
      format.js { render action: "broadcast_update_playlist" }
    end
  end

  def upvote_video
    video = Video.find(params[:video_id])

    video.liked_by current_or_guest_user
    respond_to do |format|
      format.html { redirect_to @channel }
      format.js { render action: "broadcast_update_playlist" }
    end
  end

  def unvote_video
    video = Video.find(params[:video_id])

    video.unliked_by current_or_guest_user
    respond_to do |format|
      format.html { redirect_to @channel }
      format.js { render action: "broadcast_update_playlist" }
    end
  end

  def search_video
    load_google_api
    opts = {
      q: params[:q],
      type: :video,
      videoEmbeddable: "true",
      part: "id,snippet"
    }
    response = @client.execute!(api_method: @youtube.search.list, parameters: opts)
    @search_results = []
    response.data.items.each do |item|
      @search_results.push Video.new title: item.snippet.title, uid: item.id.videoId
    end
    logger.debug @search_results
    respond_to do |format|
      format.js {}
    end
  end

  def playlist
  end

  def play_video
    respond_to do |format|
      format.js { }
    end
  end

  def next_video
    @channel.play_next_video
    flash[:notice] = "#{@channel.active_video.title} started playing" if @channel.active_video
    respond_to do |format|
      format.js { render action: "broadcast_update_active_video" }
    end
  end

  private

  def get_channel
    @channel = Channel.friendly.find(params[:id])
  end

  def load_google_api
    @client = Google::APIClient.new key: GOOGLE_DEVELOPER_KEY, application_name: 'VIDJ', application_version: '0.0.1', authorization: nil
    @youtube = @client.discovered_api('youtube', 'v3')
  end

  def channel_params
    params.require(:channel).permit(:title, :description)
  end
end
