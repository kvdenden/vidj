class Channel < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged

  validates_presence_of :title, :slug

  belongs_to :user
  has_many :videos, dependent: :destroy, after_add: :check_active

  def active_video
    videos.where(active: true).first
  end

  def playlist
    videos.where(active: nil, played_at: nil).order("cached_weighted_score DESC")
  end

  def history(limit=nil)
    videos.where(active: false).where.not(played_at: nil).order("played_at DESC").limit(limit)
  end

  def play_next_video
    active_video.update(active: false) if active_video
    next_video = playlist.first
    next_video.update(active: true, played_at: DateTime.now) if next_video
  end

  def check_active(video)
    play_next_video if active_video.nil?
  end

  def slug_candidates
    [
      :title,
      [:title, :id]
    ]
  end

  def should_generate_new_friendly_id?
    slug.blank? || title_changed?
  end
end
