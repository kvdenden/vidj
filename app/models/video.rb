class Video < ActiveRecord::Base
  belongs_to :user
  belongs_to :channel

  validates_presence_of :uid, :channel

  acts_as_votable

  def link
    "http://youtu.be/#{uid}"
  end
end
